#include "asf.h"

#define LED_PIO
#define LED_PIO_ID 12
#define LED_PIO_PIN 8
#define LED_PIO_PIN_MASK (1 << LED_PIO_PIN)
#define LED_PIO PIOC

#define BUT_PIO
#define BUT_PIO_ID 12
#define BUT_PIO_PIN 19
#define BUT_PIO_PIN_MASK (1 << BUT_PIO_PIN)
#define BUT_PIO PIOC

#define MOTOR1_PIO
#define MOTOR1_PIO_ID 10
#define MOTOR1_PIO_PIN 2
#define MOTOR1_PIO_PIN_MASK (1 << MOTOR1_PIO_PIN)
#define MOTOR1_PIO PIOA

#define MOTOR2_PIO
#define MOTOR2_PIO_ID 10
#define MOTOR2_PIO_PIN 3
#define MOTOR2_PIO_PIN_MASK (1 << MOTOR2_PIO_PIN)
#define MOTOR2_PIO PIOA

#define MOTOR3_PIO
#define MOTOR3_PIO_ID 10
#define MOTOR3_PIO_PIN 24
#define MOTOR3_PIO_PIN_MASK (1 << MOTOR3_PIO_PIN)
#define MOTOR3_PIO PIOA

#define MOTOR4_PIO
#define MOTOR4_PIO_ID 10
#define MOTOR4_PIO_PIN 4
#define MOTOR4_PIO_PIN_MASK (1 << MOTOR4_PIO_PIN)
#define MOTOR4_PIO PIOA

#define SENSOR_PIO
#define SENSOR_PIO_ID 10
#define SENSOR_PIO_PIN 21
#define SENSOR_PIO_PIN_MASK (1 << SENSOR_PIO_PIN)
#define SENSOR_PIO PIOA

int step = 0; //Qual passo o motor está
int steps = 0; //Passos dados
int direction; //1 para abrir (horário), 0 para fechar (anti-horário)
int status = 0; //0 para fechado, 1 para aberto

void setDirection(){
	if(direction == 1){
		step++;
		steps++;
	}
	if(direction == 0){
		step--;
		steps--;
	}
	if(step > 7){
		step = 0;
	}
	if(step < 0){
		step = 7;
	}
}

void stepper(){
	switch(step){
		case 0:
		pio_clear(MOTOR1_PIO, MOTOR1_PIO_PIN_MASK);
		pio_clear(MOTOR2_PIO, MOTOR2_PIO_PIN_MASK);
		pio_clear(MOTOR3_PIO, MOTOR3_PIO_PIN_MASK);
		pio_set(MOTOR4_PIO, MOTOR4_PIO_PIN_MASK);
		break;
		
		case 1:
		pio_clear(MOTOR1_PIO, MOTOR1_PIO_PIN_MASK);
		pio_clear(MOTOR2_PIO, MOTOR2_PIO_PIN_MASK);
		pio_set(MOTOR3_PIO, MOTOR3_PIO_PIN_MASK);
		pio_set(MOTOR4_PIO, MOTOR4_PIO_PIN_MASK);
		break;
		
		case 2:
		pio_clear(MOTOR1_PIO, MOTOR1_PIO_PIN_MASK);
		pio_clear(MOTOR2_PIO, MOTOR2_PIO_PIN_MASK);
		pio_set(MOTOR3_PIO, MOTOR3_PIO_PIN_MASK);
		pio_clear(MOTOR4_PIO, MOTOR4_PIO_PIN_MASK);
		break;
		
		case 3:
		pio_clear(MOTOR1_PIO, MOTOR1_PIO_PIN_MASK);
		pio_set(MOTOR2_PIO, MOTOR2_PIO_PIN_MASK);
		pio_set(MOTOR3_PIO, MOTOR3_PIO_PIN_MASK);
		pio_clear(MOTOR4_PIO, MOTOR4_PIO_PIN_MASK);
		break;
		
		case 4:
		pio_clear(MOTOR1_PIO, MOTOR1_PIO_PIN_MASK);
		pio_set(MOTOR2_PIO, MOTOR2_PIO_PIN_MASK);
		pio_clear(MOTOR3_PIO, MOTOR3_PIO_PIN_MASK);
		pio_clear(MOTOR4_PIO, MOTOR4_PIO_PIN_MASK);
		break;
		
		case 5:
		pio_set(MOTOR1_PIO, MOTOR1_PIO_PIN_MASK);
		pio_set(MOTOR2_PIO, MOTOR2_PIO_PIN_MASK);
		pio_clear(MOTOR3_PIO, MOTOR3_PIO_PIN_MASK);
		pio_clear(MOTOR4_PIO, MOTOR4_PIO_PIN_MASK);
		break;
		
		case 6:
		pio_set(MOTOR1_PIO, MOTOR1_PIO_PIN_MASK);
		pio_clear(MOTOR2_PIO, MOTOR2_PIO_PIN_MASK);
		pio_clear(MOTOR3_PIO, MOTOR3_PIO_PIN_MASK);
		pio_clear(MOTOR4_PIO, MOTOR4_PIO_PIN_MASK);
		break;
		
		case 7:
		pio_set(MOTOR1_PIO, MOTOR1_PIO_PIN_MASK);
		pio_clear(MOTOR2_PIO, MOTOR2_PIO_PIN_MASK);
		pio_clear(MOTOR3_PIO, MOTOR3_PIO_PIN_MASK);
		pio_set(MOTOR4_PIO, MOTOR4_PIO_PIN_MASK);
		break;
		
		default:
		pio_clear(MOTOR1_PIO, MOTOR1_PIO_PIN_MASK);
		pio_clear(MOTOR2_PIO, MOTOR2_PIO_PIN_MASK);
		pio_clear(MOTOR3_PIO, MOTOR3_PIO_PIN_MASK);
		pio_clear(MOTOR4_PIO, MOTOR4_PIO_PIN_MASK);
		break;
	}
	setDirection();
}

void led_on(void){
	pio_clear(LED_PIO, LED_PIO_PIN_MASK);
}

void led_off(void){
	pio_set(LED_PIO, LED_PIO_PIN_MASK);
}

void open(){
	direction = 1;
	led_on();
	while (status == 0){
		delay_us(900);
		stepper();
    }
	led_off();
}

void close(){
	direction = 0;
	led_on();
	while (steps > 0){
		delay_us(900);
		stepper();
	}
	status = 0;
	led_off();
}

void setStatus_callback(void){
	status = 1;
}

int main(void){
	
	sysclk_init();
	WDT->WDT_MR = WDT_MR_WDDIS;
	
	pmc_enable_periph_clk(BUT_PIO_ID);
	pmc_enable_periph_clk(LED_PIO_ID);
	pmc_enable_periph_clk(SENSOR_PIO_ID);
	pmc_enable_periph_clk(MOTOR1_PIO_ID);
	pmc_enable_periph_clk(MOTOR2_PIO_ID);
	pmc_enable_periph_clk(MOTOR3_PIO_ID);
	pmc_enable_periph_clk(MOTOR4_PIO_ID);
	
	//Configuração dos pinos
	pio_configure(LED_PIO, PIO_OUTPUT_0, LED_PIO_PIN_MASK, PIO_DEFAULT);
	pio_configure(BUT_PIO, PIO_INPUT, BUT_PIO_PIN_MASK, PIO_PULLUP);
	pio_configure(SENSOR_PIO, PIO_INPUT, SENSOR_PIO_PIN_MASK, PIO_DEFAULT);
	pio_configure(MOTOR1_PIO, PIO_OUTPUT_0, MOTOR1_PIO_PIN_MASK, PIO_DEFAULT);
	pio_configure(MOTOR2_PIO, PIO_OUTPUT_0, MOTOR2_PIO_PIN_MASK, PIO_DEFAULT);
	pio_configure(MOTOR3_PIO, PIO_OUTPUT_0, MOTOR3_PIO_PIN_MASK, PIO_DEFAULT);
	pio_configure(MOTOR4_PIO, PIO_OUTPUT_0, MOTOR4_PIO_PIN_MASK, PIO_DEFAULT);

	pio_enable_interrupt(SENSOR_PIO, SENSOR_PIO_PIN_MASK);
	pio_handler_set(SENSOR_PIO, SENSOR_PIO_ID, SENSOR_PIO_PIN_MASK, SENSOR_PIO_PIN, setStatus_callback);

	NVIC_EnableIRQ(SENSOR_PIO_ID);
	NVIC_SetPriority(SENSOR_PIO_ID, 0);
	
	while (1){
		//Gaveta inicia fechada
		int but_status_0 = pio_get(BUT_PIO, PIO_INPUT, BUT_PIO_PIN_MASK);

		//Botão ativado (0) e Gaveta fechada (0) -> Gaveta abre (open)
		if(but_status_0 == 0 && status == 0){
			open();
		}

		//Quando o sensor de obstáculo for desativado (abriu até o máximo), status = 1 -> Será feito por interrupção
		//Status 1 -> Gaveta aberta, motor parado.
		int but_status_1 = pio_get(BUT_PIO, PIO_INPUT, BUT_PIO_PIN_MASK);
		
		//Botão ativado (0) e Gaveta aberta (1) -> Gaveta fecha (close)
		if(but_status_1 == 0 && status == 1){
			close();
		}
	
	}
	return 0;
}

//https://github.com/arduino/Arduino/blob/master/libraries/Stepper/src/Stepper.cpp
//https://github.com/arduino/Arduino/blob/master/libraries/Stepper/src/Stepper.h
