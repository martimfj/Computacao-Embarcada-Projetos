# Gaveta automática
#### por Martim Ferreira José e Lucas Chen Alba

### Descrição
Este projeto tem como objetivo introduzir o kit de desenvolvimento de microcontroladores, através da criação de um projeto que utilize o mundo externo como interface. Para isto, o projeto precisa ter ao menos duas entradas e duas saídas digitais distintas.

Como projeto, foi criado uma gaveta automática, que abre e fecha ao toque de dois botões. O botão verde aciona motor de passo, que gira a engrenagem sobre a cramalhera, deslocando a gaveta como um todo. Um pouco antes do ponto crítico, onde queremos que a gaveta pare de abrir, há um sensor de  obstáculo que detecta se ela está aberta. Quando aberta, a gaveta pode ser fechada pelo botão vermelho. Os LEDs verde e vemelho piscam de acordo com a o estado do  projeto, abrindo e fechando respectivamente.


#### Diagrama de blocos
![Diagrama de Blocos](diagrama_de_blocos.png)

### Lista de materiais
- 1 SAM E70
- Motor de passo                            (pinos 2, 3, 4 e 24 do PIOA)
- 1 LED                                     (da placa, pino 8 PIOC)
- 1 sensor de obstáculo infravermelho       (pino 21 PIOA)
- 1 botão verde                             (pino 19 PIOC)
- protoboard pequena
- jumpers

### Funcionamento
Para o projeto utilizamos o kit de desenvolvimento SAM E70 Xplained, o qual é montado no microcontrolador ATSAME70Q21 que por sua vez possui o processador Cortex M7.

Progamação:
Nosso projeto utiliza tanto o loop principal (while (1)) como interrupções.
O loop principal checa constantemente o estado do botão e do sensor infravermelho. Caso o botão seja apertado, a função open() é chamada, esta por sua vez, aciona o LED, define a direção como sendo 1 e chama a função stepper() dentro de um loop com a condição sendo o sensor infravermelho estar desligado. A função stepper roda o motor de acordo com a direção definida anteriormente, e adiciona 1 passo à variável steps. Assim que o sensor infravermelho ligar (como ele é controlado pela função interrupt, assim que o estado dele mudar (de 0 para 1), a função callback será chamada, redefinindo status para 1), com isso a função stepper() termina, o LED é desligado na função open(), terminando assim a chamada da função open().

O motor de passo é controlado na função stepper(). O que ela faz é repetir uma sequência aos pinos do motor, rotacionando-o.


### Vídeo de validação
https://youtu.be/c5ufU_JyIJg
